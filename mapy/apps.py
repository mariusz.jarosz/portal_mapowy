from django.apps import AppConfig


class MapyConfig(AppConfig):
    name = 'mapy'
