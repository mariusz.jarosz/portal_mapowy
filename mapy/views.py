from django.shortcuts import render
from django.views.generic import TemplateView
from django.core.serializers import serialize
from django.http import HttpResponse
from .models import GranicaGminy, Punkty, GraniceMiasta, ObszarZdegradowany

# Create your views here.
class HomePageView(TemplateView):
	template_name = 'index.html'

def granica_gminy(request):
	granice = serialize('geojson', GranicaGminy.objects.all())
	return HttpResponse(granice, content_type='json')

def punkty_data(request):
	points = serialize('geojson', Punkty.objects.all())
	return HttpResponse(points, content_type='json')

def granica_miasta(request):
	granice= serialize('geojson', GraniceMiasta.objects.all())
	return HttpResponse(granice, content_type='json')

def obszar_zdegradowany(request):
	strefy = serialize('geojson', ObszarZdegradowany.objects.all())
	return HttpResponse(strefy, content_type='json')