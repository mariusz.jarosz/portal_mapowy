from django.db import models
from django.contrib.gis.db import models as gismodels
# Create your models here.

class Punkty(gismodels.Model):
	name = models.CharField(max_length=80)
	location = gismodels.PointField(srid=2180)
	# objects = gismodels.GeoManager()

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name_plural = " Punkty"

class GranicaGminy(gismodels.Model):
    name = models.BigIntegerField()
    geom = gismodels.MultiLineStringField(srid=2180)

    def __unicode__(self):
    	return self.id
    class Meta:
        verbose_name_plural = " Granica Gminy"

class GraniceMiasta(gismodels.Model):
    name = models.BigIntegerField()
    geom = gismodels.MultiLineStringField(srid=2180)

    def __unicode__(self):
        return self.id
    class Meta:
        verbose_name_plural = " Granica miasta"

class ObszarZdegradowany(gismodels.Model):
    name = models.BigIntegerField()
    nazwa2 = models.CharField(max_length=25)
    geom = gismodels.MultiPolygonField(srid=2180)

    def __unicode__(self):
        return self.id
    class Meta:
        verbose_name_plural = " Obszar zdegradowany"
