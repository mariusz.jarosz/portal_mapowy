import os
from django.contrib.gis.utils import LayerMapping
from .models import GranicaGminy, GraniceMiasta, ObszarZdegradowany


# granicagminy_mapping = {
#     'name' : 'id',
#     'geom' : 'MULTILINESTRING',
# }

# granicagminy_shp = os.path .abspath(os.path.join(os.path.dirname(__file__),'data/shp/Granice/granicagminy.shp'))

# def run(verbose=True):
# 	lm = LayerMapping(GranicaGminy, granicagminy_shp, granicagminy_mapping, transform= True, encoding='iso-8859-1')
# 	lm.save(strict=True, verbose=verbose)


# granicemiasta_mapping = {
#     'name' : 'id',
#     'geom' : 'MULTILINESTRING',
# }

# granicemiasta_shp = os.path .abspath(os.path.join(os.path.dirname(__file__),'data/shp/Granice/granicamiasta.shp'))

# def run(verbose=True):
# 	lm = LayerMapping(GraniceMiasta, granicemiasta_shp, granicemiasta_mapping, transform= False, encoding='iso-8859-1')
# 	lm.save(strict=True, verbose=verbose)


obszarzdegradowany_mapping = {
    'name' : 'id',
    'nazwa2' : 'nazwa2',
    'geom' : 'MULTIPOLYGON',
    # 'geom' : 'POLYGON',
}

obszarzdegradowany_shp = os.path .abspath(os.path.join(os.path.dirname(__file__),'data/shp/Obszar_zdegradowany/ObszarZdegradowany.shp'))


def run(verbose=True):
	lm = LayerMapping(ObszarZdegradowany, obszarzdegradowany_shp, obszarzdegradowany_mapping, transform= False, encoding='iso-8859-1')
	lm.save(strict=True, verbose=verbose)
