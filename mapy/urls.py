from django.conf.urls import include, url
from .views import HomePageView, granica_gminy, punkty_data, granica_miasta, obszar_zdegradowany

urlpatterns = [
    url(r'^$', HomePageView.as_view(), name = 'home'),
    url(r'^granicagminy/$', granica_gminy, name = 'granicagminy'),
    url(r'^granicamiasta/$', granica_miasta, name = 'granicamiasta'),
    url(r'^obszarzdegradowany/$', obszar_zdegradowany, name = 'obszarzdegradowany'),
    url(r'^punkty_data/$', punkty_data, name = 'punkty'),
]