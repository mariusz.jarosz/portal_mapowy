from django.contrib import admin
from .models import Punkty, GranicaGminy, GraniceMiasta, ObszarZdegradowany
# from django.contrib.gis.db import OSMGeoAdmin
from leaflet.admin import LeafletGeoAdmin
# Register your models here.

class PunktyAdmin(LeafletGeoAdmin):
	list_display = ('name', 'location')


class GranicaAdmin(LeafletGeoAdmin):
	list_display = ('name', 'geom')

class GraniceMiastaAdmin(LeafletGeoAdmin):
	list_display = ('name', 'geom')

class ObszarZdegradowanyAdmin(LeafletGeoAdmin):
	list_display = ('name','nazwa2')


admin.site.register(Punkty, PunktyAdmin)

admin.site.register(GranicaGminy, GranicaAdmin)

admin.site.register(GraniceMiasta, GraniceMiastaAdmin)

admin.site.register(ObszarZdegradowany, ObszarZdegradowanyAdmin)